## Genomon

Genomon is the client library for accessing bounded time provided by clockd, clockd is not publicly available at this stage. 

[Genomon](https://en.wikipedia.org/wiki/Gnomon) is the part of a sundial used for projecting a pinhole shadow image of the sun to tell the time of day. 

## Languages and platforms

Golang is natively supported, C support will be provided soon so C++, Python and Rust can be transparently supported. 

Linux is support, with Darwin supported for development and testing purposes.

## LICENSE

Genomon is the Apache2 licensed.
